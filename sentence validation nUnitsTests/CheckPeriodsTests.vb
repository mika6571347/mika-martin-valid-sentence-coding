Imports NUnit.Framework
Imports sentence_validation

Namespace sentence_validation_nUnitsTests

    Public Class CheckPeriodsTests

        Private CheckPeriodsVar As New CheckPeriods


        <SetUp>
        Public Sub Setup()
            CheckPeriodsVar = New CheckPeriods

        End Sub

        <TestCase("Hello there.")> ' should pass
        <TestCase("How. have you been")> ' should not pass
        <TestCase("How are things")> ' should not pass
        Public Sub validateTest(ByVal TestString As String)

            'Assign

            Dim TestStringArray() As String = TestString.Split(" ")

            '
            'TestString(0) = "Hello"
            'TestString(1) = "how"
            'TestString(2) = "are"
            'TestString(3) = "you?"
            'Act
            Dim result As Boolean = CheckPeriodsVar.validate(TestStringArray)

            'Setup
            Assert.AreEqual(True, result)


            'Assert.Pass()
        End Sub

    End Class

End Namespace